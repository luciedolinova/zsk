<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Nový zimní stadion v Kuřimi nabízí dvě lední plochy pro hokej i bruslení.">
  <meta name="keywords" content="hokej,Kurim,stadion,zimni,brusleni">
  
   
    <meta property="og:title" content="Zimní stadion Kuřim"/>
    <meta property="og:url" content="http://www.zimnistadionkurim.cz"/>
    <meta property="og:site_name" content="Zimní stadion Kuřim"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://www.zimnistadionkurim.cz/images/fb_name.png"/>
    <meta property="og:image:width" content="10px"/>
    <meta property="og:image:height" content="5px" />
  
  <title>Zimní stadion Kuřim</title>
 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  
  <link href="https://fonts.googleapis.com/css?family=Montserrat|Roboto" rel="stylesheet">

	<link rel="stylesheet" href="src/css/swipebox.css">
	<link rel="stylesheet" type="text/css" href="style0829.css">

</head>
