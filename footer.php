 <div class="col-xs-12 footer">
          <!-- footer-->
          <div class="container-fluid whole">
            <div class="row ">
              <div class="col-xs-6">

                <div id="partneri">
                  <h4>Partneři</h4>
                  <div class="container-fluid">
                    <div class="row ">



                      <div class="col-md-6 links">


                        <a href="https://www.kurim.cz/"><img class="kurim"src="images/logo_kurim.png" width=100px alt="Mesto Kurim"></a>
                        <a href="http://www.hhtechnika.cz/">H+H Technika</a>
                        <a href="https://www.starha.almadeo.cz/">Štarha Engineering s.r.o.</a>
                      </div>
                      <div class="col-md-6">
                        <div class="wellness links dva">
                          <a class="big" href="http://www.wellnesskurim.cz"><img class="big" src="images/logo_wellness.png" width=200px alt="Logo Wellness Kurim"></a>

                          <a class="small" href="http://www.wellnesskurim.cz"><img class="small" src="images/logo_wellness_small.png" width=135px alt="Logo Wellness Kurim"></a>
                          <a href="http://www.knesl-kyncl.com/">Knesl Kynčl architekti s.r.o.</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-6">
                <div class="media">
                  <h4>Média</h4>
                  <div class="container-fluid">
                    <div class="row media">
                      <div class="col-md-6">
                        <a href="http://kurimskymagazin.cz/">Kuřimský magazín</a><br>
                        <a href="http://tvkotva-kurim.cz/">TV Kotva</a><br>
                        <a href="http://www.ceskatelevize.cz/sport/">ČT Sport</a>

                      </div>
                      <div class="col-md-6">
                        <a href="http://www.cslh.cz/">Český svaz ledního hokeje</a> <br>
                        <a href="https://www.livesport.cz/hokej/">Live Sport - výsledky</a>
                        <a href="https://www.tipsport.cz/">Tipsport</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="sponzori">

            <a href="http://en.khl.ru/"><img class="logo 2"src="images/khl.png" width=74px alt="KHL"></a>
            <a href="https://www.nhl.com/cs"><img class="logo 3"src="images/nhl.png"  width=40px alt="NHL"></a>
            <a href="http://www.msmt.cz/"><img class="logo 6"src="images/msmt.png" width=70px alt="MŠMT"></a>
            <a href="http://www.hc-kometa.cz/"><img class="logo 4"src="images/kometa.png" width=60px alt="KOMETA"></a>
            <a href="https://www.championshockeyleague.com/en"><img class="logo 5"src="images/chl.png" width=35px alt="CHL"></a>
          </div>

        </div>
        <!-- footer-->
        