<!DOCTYPE html>
<html>
<?php
include_once("head.php")
?>


<body data-spy="scroll" data-target=".navbar" data-offset="50">
  <div class="body">
    <div class="hlavicka">
      <div class="container">


        <!-- Modal -->
        <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">REZERVACE</h4>
              </div>
              <div class="modal-body">
                <p> Již brzy bude možné si rezervovat ledovou plochu online.<br> Nyní volejte na tel.: +420 604 239 942. Děkujeme.</p>

                <img class="rezervaceimg" src="images/rezervace.jpg" style="max-width:880px;height:100%" alt="Rezervace">

              </div>
            </div>
          </div>
        </div>



        <div class="col-md-12 nazev">
          <img id=name src="images/zsklogo1.png" alt="Zimni stadion Kuřim" style="max-width:480px;height:auto">




        </div>

      </div>


      <div class="menu">
        <nav id=nav class="navbar navbar-default" data-spy="affix" data-offset-top="120">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
                <span class="ico ico-burger-menu border">
                  <span class="sr-only">Přepnout menu</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>


              <a class="navbar-brand" href="#">

                  <button type="button" class="btn btn-primary btn-lg mobile" style="float:left" data-toggle="modal" data-target="#myModal">
                    Rezervace
                  </button>


                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id=menu class="collapse navbar-collapse">
              <ul class="nav nav-justified">
                <li class=domu><a href="#name"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Stadion </a>
                  <ul class="dropdown-menu">
                    <li><a href="#projekt">Projekt</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#technologie">Technologie</a></li>
                    <li role="separator" class="divider"></li>

                    <li><a href="#harmonogram">Harmonogram</a></li>

                  </ul>
                </li>
                <li class="dropdown spec">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sportovní areál</a>
                  <ul class="dropdown-menu">
                    <li><a href="http://www.halakurim.cz/">Městská sportovní hala</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="spec"><a href="http://www.wellnesskurim.cz">Wellness Kuřim</a></li>
                    <li role="separator" class="divider"></li>

                    <li><a href="http://fckurim.cz">Fotbalový stadion Kuřim</a></li>
                  </ul>
                </li>
                <li class="spec"><a href="#mystery">Kuřimský hokej</a></li>
                <li><a href="#foto">Foto</a></li>
                <li><a href="#partneri">Partneři</a></li>
                <li><a href="#kontakt">Kontakt</a></li>
              </ul>




            </div>
            <!-- /.navbar-collapse -->
          </div>
          <!-- /.container-fluid -->
        </nav>
      </div>
    </div>










    <div class="wrap">
      <!-- Carousel -->
      <div class="banner">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="images/foto1.jpg" alt="Stadion" style="width:100%;max-height:486px;">
              <!--<div class="carousel-caption">
                <h3>Vizualizace nové haly</h3>

              </div>-->

            </div>
            <div class="item">
              <img src="images/foto2.jpg" alt="Stadion" style="width:100%;max-height:486px;">
              <!--<div class="carousel-caption">
              <h3>Atletický stadion</h3>

            </div>-->

            </div>

            <div class="item">
              <img src="images/foto3.jpg" alt="Stadion" style="width:100%;max-height:486px;">
              <!--<div class="carousel-caption">
            <h3>S tátou na hokej...</h3>

          </div>-->

            </div>

            <div class="item">
              <img src="images/foto4.jpg" alt="Stadion" style="width:100%;max-height:486px;">
              <!--<div class="carousel-caption">
          <h3>S tátou na hokejł...</h3>

        </div>-->

            </div>
          </div>
          <!-- Left and right controls -->
          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
        </div>

      </div>


      <!-- OBSAH -->
      <div id=d class="content">
        <div class="row">




          <div class="col-xs-12 col-sm-7 levy">


            <!-- LEVY SLOUPEC -->
            <div class="col-xs-12 projekt">
              <div class="inner 1">
                <h2 id="stadion">

            Stadion</h2>

                <h3 id="projekt" class="icon">Projekt</h3>
                <p>Společnost ZIMNÍ STADION KUŘIM s.r.o. představuje projekt „Hokejové haly“ určený zejména pro mládežnický a rekreační lední hokej. Nové sportovní centrum se bude skládat z jedné ledové plochy, multifunkční kryté haly, tribuny pro cca 350
                  lidí a velkého ochozu nad ledovou plochou. <a href="#projekt" id="vice">Více</a></p>
                <div class="projekt-obsah">

                  V rámci celé investice naše společnost vybuduje nová parkovací stání mezi plánovaným parkovištěm městské sportovní haly a stávajícím parkovištěm pro zákazníky wellness centra. Podílíme se také na výstavbě areálové retenční nádrže a na spolufinancování
                  úprav zelených ploch přilehlého okolí.<br>
                  <br> Město Kuřim tak získá dalších cca 100 parkovacích stání určených pro veřejnost. V těsném okolí hokejové haly budou zřízeny i dvě autobusové zastávky přímo u vstupu do zimního stadionu.<br>
                  <br> Tento projekt bude realizován ve spolupráci s městem Kuřim, firmou Sport Hotel Kuřim s.r.o a naší společností, což bylo stvrzeno, po více jak 1,5 letém jednání, podpisem dne 12.5.2017.
                  <a href="#projekt" id="mene">Méně</a>

                  </p>

                </div>

                <h3 id="technologie" class="icon">Technologie</h3>

                <p>Srdcem zimního stadionu je technologie mrazení, chlazení a vytápění, kterou nám zajišťuje firma H+H Technika s.r.o. Stadion bude využívat moderní systém nepřímého chlazení na vychlazování ledové plochy s využitím sněžné jámy a bude
                  možné jej využívat po celý rok. <a href="#technologie" id="vice2">Více</a>
                  <p>

                    <div class="technologie-obsah">


                      <p>Nepřímé chlazení spočívá v tom, že chladicí stroj umístěný ve strojovně chladí nemrznoucí kapalinu, která je čerpadly rozvedena pod ledové plochy. Kondenzační teplo bude využito pro rozpouštění ledu ve sněžné jámě a ohřev vody pro
                        sněžnou rolbu.<br>
                        <br> Chladicí zařízení pracuje plně automaticky a nevyžaduje trvalou obsluhu. </p>

                      <h4>Ledová plocha – Hala </h4>
                      <p>Doporučená/ max. výška ledu 30 mm/ 50 mm <br> Teplota ledu -3°C až -6°C<br> Teplota vzduchu nad plochou + 4°C až + 8°C<br> RH vzduchu nad plochou 65%<br> Ledová plocha – provozní stav -5°C +28°C (+35°C) 410 kW</p><br>
                                     


                      <h4>Sněžná jáma</h4>
                      <p>Objem vody sněžné jámy 18m3<br> Denní zátěž (počet úprav ledové plochy) 24x<br> Celkové nároky na tepelnou energii 1800kW</p>

                      <p> <a href="#technologie" id="mene2">Méně</a> </p>


                    </div>


                    
                    <div class="galeryFoto clearfix timeline">
                    <h3 id="harmonogram" class="icon">Harmonogram</h3>
                      
                      
                      <ul>
                      <li class="horizontal" >
                        <a class="swipebox" href="images/timeline_big.svg" data-toggle="lightbox" data-gallery="timeline">
                        <img class="timeline" src="images/timeline.svg" alt="harmonogram" width="100%" margin="auto">
                        </a>
                        </li>
                      
                      
                      </ul>
                      </div>
                      <div class="vertical">
                        <!--<a class=" vertical " href="images/timelineVert.svg" data-toggle="lightbox" data-gallery="timeline vert"></a>-->
                        <img class="timeline" src="images/timelineVert.svg" alt="harmonogram" height="auto" margin="auto">
                        
                        </div>
                      
                      
                      
                      
                      





                    
                    <!--<img class="timeline" src="timeline.svg" alt="harmonogram" width="100%" margin="auto">
                    <img class="timeline2" src="images/timeline2.png" alt="harmonogram" width="60%">-->


                    <div class="prazdny">
                      <p>
                        <br></p>

                    </div>
              </div>
            </div>
          </div>
          <!-- LEVÝ SLOUPEC -->



          <div class="col-xs-12 col-sm-5 pravy">
            <!-- PRAVÝ SLOUPEC -->


            <div class="col-xs-12 rezervace">
              <div class="inner">





                <div class="container-fluid">
                  <a href="https://www.facebook.com/zimnistadionkurim/?fref=ts"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>

                  <button type="button" class="btn btn-primary btn-lg desktop" style="float:right" data-toggle="modal" data-target="#myModal">
                          Rezervace
                        </button>







                </div>
              </div>










              <div class="col-xs-12 kamera">
                <!-- KAMERA -->
                <div class="inner">


                  <h3>Živě</h3>


                  <img width="100%" max-height="270px" src="images/jizbrzy.jpg">

                </div>

                <div class="col-xs-12 aktuality">
                  <!-- AKTUALITY -->
                  <div class="inner">


                    <div class="row">

                      <h3 class="odkazaktual">
                              <a href="aktuality.php#aktuality">Aktuality</a>
                            </h3>
                      <div class="col-xs-12">
                        <div class="media aktualita">
                          <div class="media-left">
                            <img src="images/vizu11.jpg" width="100" height="100" alt="Stavba nové haly">
                          </div>

                          <div class="media-body">
                            <h4>
                                    <a href="aktuality.php#aktuality">Sousední hala Míčových sportů dostává nový kabát</a></h4>
                            <p>28.6.2017</p>

                          </div>
                        </div>
                      </div>

                      <div class="col-xs-12">
                        <div class="media aktualita">
                          <div class="media-left">
                            <img src="images/stavba.jpg" width="100" height="100" alt="Kamera">
                          </div>

                          <div class="media-body">
                            <h4>
                                      <a href="aktuality.php#aktuality">Nová kamera bude snímat pokroky ve stavbě stadionu</a> </h4>
                            <p>14.6.2017</p>

                          </div>
                        </div>
                      </div>
                      <!-- col xs -->
                      <div class="blok dalsi">

                        <div class="next">


                          <a class="dalsi" href="aktuality.php#aktuality">Další</a> <br>
                          <a class="odkaz" href="aktuality.php#aktuality">
                                      <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                                    </a>
                        </div>

                      </div>

                    </div>
                    <!-- row -->
                  </div>
                </div>
                <!-- AKTUALITY -->
              </div>
            </div>
            <!-- PRAVÝ SLOUPEC -->
          </div>
          <!-- row-->
        </div>
        <!-- content-->



        <div id="mystery" class="col-xs-12 HCMystery">

          <h3 class="icon">Historie kuřimského hokeje</h3>
          <div class="col-xs-12 col-sm-8 text">

            

            <p>Kuřimský hokej to nebyla jen obyčejná rybniční liga. Do 60. let si připsali hned několik úspěšných ročníků.<br> <br>Letos Kuřim oslaví významné sportovní výročí. Hokej se totiž začal hrát na kluzišti u Sokolovny, a později na rybníku za zámkem,
              už před 75 lety. <br> Pro více informací navštivte facebookové stránky klubu.



            </p>
            <a href="https://www.facebook.com/Mystery-Ku%C5%99im-na-brusl%C3%ADch-1133545816791637/?fref=ts"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
          </div>

          <div class="col-xs-12 col-sm-4 logo-mystery">
            <img src="images/mystery.png" width="190px" alt="logo Mystery">

          </div>

        </div>


        <div id="foto" class="col-xs-12 fotogalerie">

          <h3 class="icon"> <a href="fotogalerie.php#galerie">Fotogalerie</a></h3>


          <div class="wrapper">



            <div class="box stadion">
              <a href="stadion.php#galerie">
                          <span class="caption fade-caption jedna ">
                            <h3>Stadion</h3>
                          </span></a>
            </div>

            <div class="box hokejkurim">
              <a href="hokejkurim.php#galerie">
                            <span class="caption fade-caption dva">
                              <h3>Historie hokeje v Kuřimi</h3>
                            </span></a>
            </div>
            <div class="box mystery ">
              <a href="hcmystery.php#galerie">
                              <span class="caption fade-caption dva">
                                <h3>HC Mystery</h3>
                              </span></a>
            </div>
            <div class="box stavba ">
              <a href="stavba.php#galerie">
                                <span class="caption fade-caption dva">
                                  <h3>Výstavba areálu</h3>
                                </span></a>
            </div>




          </div>


        </div>








        <div class="col-xs-12 kontakty">


          <div class="col-xs-12 adresa">
            <!-- Kontakt-->
            <div class="row">

              <h2 class="icon" id=kontakt>Kontakt</h2>
              <div class="col-xs-12 col-sm-6 kontakt">
                <h3>Zimní stadion Kuřim,&nbsp s.r.o.</h3>
                <p> Kancelář:<br> Dlouhá 1720/8 <br> 664 34 Kuřim<br> email: info@hokejvkurimi.cz <br> Tel.: +420 604 239 942<br> IČ: 05 886 007<br> DIČ: CZ05 886 007<br>



                </p>
              </div>

              <div class="col-xs-12 col-sm-6 mapa">


                <iframe class="googlemaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2602.1339266870923!2d16.540739214869205!3d49.29280607933242!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4712904f6bd9cf11%3A0x728dea233c496345!2zRGxvdWjDoSAxNzIwLCA2NjQgMzQgS3XFmWlt!5e0!3m2!1scs!2scz!4v1499361057263"
                  width="100%" height="270" frameborder="0" style="border:0" allowfullscreen></iframe>

              </div>
            </div>
          </div>
          <!-- Kontakt-->
        </div>



       <?php
include_once("footer.php")
?>




                 
                    







        <div class="bla">
          <div class="copy">
            &copy; Zimní stadion Kuřim, 2017
          </div>
        </div>



      </div>
      <!-- wrap-->




    </div>
    <!-- body-->



                  



                          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                          <script src="src/js/jquery.swipebox.js"></script>
                          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

                          <script src="script.js"> </script>
                          <script type="text/javascript">
                            ;( function( $ ) {

                              $( '.swipebox' ).swipebox({
                                removeBarsOnMobile : true, // false will show top bar on mobile devices
                              hideBarsDelay : 300000, //
                              });
                            } )( jQuery );
                            </script>


    
   



</body>

</html>
