<!DOCTYPE html>
<html>
<?php
include_once("head.php")
?>
<body >
	<div class="body">
		<div class="hlavicka">
			<div class="container">


         <!-- Modal -->
                <div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">REZERVACE</h4>
                  </div>
                  <div class="modal-body">
                    <p> Již brzy bude možné si rezervovat ledovou plochu online. Nyní volejte na tel.: +420 604 239 942. Děkujeme.</p>

                    <img class="rezervaceimg" src="images/rezervace.jpg" style="max-width:880px;height:100%" alt="Rezervace">

                  </div>
                </div>
                </div>
                </div>


				<div class="col-md-12 nazev">
					<a href="index.php#name">
					<img id=name src="images/zsklogo1.png" alt="Zimni stadion Kuřim" style="max-width:480px;height:auto">
					</a>




				</div>

			</div>


			<div class="menu">
				<nav id=nav class="navbar navbar-default" data-spy="affix" data-offset-top="100" >
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu" aria-expanded="false">
								<span class="ico ico-burger-menu border">
									<span class="sr-only">Přepnout menu</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>


								<a class="navbar-brand" href="#">

                  <button type="button" class="btn btn-primary btn-lg mobile" style="float:left" data-toggle="modal" data-target="#myModal">
                    Rezervace
                  </button>


                </a>
							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div id=menu class="collapse navbar-collapse">
	              <ul class="nav nav-justified">
	                <li class=domu><a href="index.php#name"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>

	                <li class="dropdown">
	                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Stadion </a>
	                  <ul class="dropdown-menu">
	                    <li><a href="#projekt">Projekt</a></li>
	                    <li role="separator" class="divider"></li>
	                    <li><a href="#technologie">Technologie</a></li>
	                    <li role="separator" class="divider"></li>

	                    <li><a href="#harmonogram">Harmonogram</a></li>

	                  </ul>
	                </li>
	                <li class="dropdown spec">
	                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sportovní areál</a>
	                  <ul class="dropdown-menu">
	                    <li><a href="http://www.halakurim.cz/">Městská sportovní hala</a></li>
	                    <li role="separator" class="divider"></li>
	                    <li class="spec"><a href="http://www.wellnesskurim.cz">Wellness Kuřim</a></li>
	                    <li role="separator" class="divider"></li>

	                    <li><a href="http://fckurim.cz">Fotbalový stadion Kuřim</a></li>
	                  </ul>
	                </li>
	                <li class="spec"><a href="index.php#mystery">Kuřimský hokej</a></li>
	                <li><a href="fotogalerie.php#galerie">Foto</a></li>
	                <li><a href="#partneri">Partneři</a></li>
	                <li><a href="index.php#kontakt">Kontakt</a></li>
	              </ul>




	            </div>
						</div><!-- /.container-fluid -->
					</nav>
				</div>
			</div>











			<div class="wrap">
				<!-- Carousel -->
				<div class="banner">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
							<li data-target="#myCarousel" data-slide-to="3"></li>
						</ol>


						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img src="images/foto1.jpg" alt="Stadion" style="width:100%;max-height:486px;">
								<!--<div class="carousel-caption">
								<h3>Vizualizace nové haly</h3>

							</div>-->

						</div>
						<div class="item">
							<img src="images/foto2.jpg" alt="Stadion" style="width:100%;max-height:486px;">
							<!--<div class="carousel-caption">
							<h3>Atletický stadion</h3>

						</div>-->

					</div>

					<div class="item">
						<img src="images/foto3.jpg" alt="Stadion" style="width:100%;max-height:486px;">
						<!--<div class="carousel-caption">
						<h3>S tátou na hokej...</h3>

					</div>-->

				</div>

				<div class="item">
					<img src="images/foto4.jpg" alt="Stadion" style="width:100%;max-height:486px;">


			</div>
		</div>

		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>

</div>


<!-- OBSAH -->
<div id=d class="content">
	<div class="row">




		<div class="col-xs-12 col-sm-7 levy">


			<!-- LEVY SLOUPEC -->
			<div class="col-xs-12 projekt">
				<div class="inner 1">
					<h3  class="icon" id="galerie">

						<a href="index.php#name">Domů</a>   </h3>


              <h3 id="galerie">Fotogalerie</h3>




					<div class="wrapper">



						<div class="box stadion">
							<a href="stadion.php#galerie">
								<span class="caption fade-caption jedna ">
									<h3>Stadion</h3>

								</span></a>
							</div>

							<div class="box hokejkurim">
								<a href="hokejkurim.php#galerie">
									<span class="caption fade-caption dva">
										<h3>Historie hokeje v Kuřimi</h3>
									</span></a>
								</div>
								<div class="box mystery ">
									<a href="hcmystery.php#galerie">
										<span class="caption fade-caption dva">
											<h3>HC Mystery</h3>
										</span></a>
									</div>
									<div class="box stavba ">
										<a href="stavba.php#galerie">
											<span class="caption fade-caption dva">
												<h3>Výstavba areálu</h3>
											</span></a>
										</div>




									</div>




						<div class="prazdny">
							<p><br><br>
							<br></p>

						</div>
					</div>
				</div>
			</div> <!-- LEVÝ SLOUPEC -->



			<div class="col-xs-12 col-sm-5 pravy"> <!-- PRAVÝ SLOUPEC -->


				<div class="col-xs-12 rezervace">
					<div class="inner">


						<div class="container-fluid">

							<a href="https://www.facebook.com/zimnistadionkurim/?fref=ts"><i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>


						<button type="button" class="btn btn-primary btn-lg desktop" style="float:right" data-toggle="modal" data-target="#myModal">
                          Rezervace
                        </button>

							</div>
					</div>


					<div class="col-xs-12 kamera"> <!-- KAMERA -->
						<div class="inner">


							<h3>Živě</h3>

              	<img width="100%" max-height="270px" src="images/jizbrzy.jpg">
						</div>

						<div class="col-xs-12 aktuality"> <!-- AKTUALITY -->
							<div class="inner">


								<div class="row">

									<h3 class="odkazaktual">
										<a href="aktuality.php#aktuality">Aktuality</a>
									</h3>
									<div class="col-xs-12">
										<div class="media aktualita">
											<div class="media-left">
												<img   src="images/vizu3.jpg" width="100" height="100" alt="Stavba nového stadionu">
											</div>

											<div class="media-body">
												<h4>
													<a href="aktuality.php#aktuality">Sousední hala Míčových sportů dostává nový kabát</a></h4>
													<p>28.6.2017</p>

												</div>
											</div>
										</div>

										<div class="col-xs-12">
											<div class="media aktualita">
												<div class="media-left">
													<img   src="images/stavba.jpg" width="100" height="100" alt="Kamera">
												</div>

												<div class="media-body">
													<h4>
														<a href="aktuality.php#aktuality">Nová kamera bude snímat pokroky ve stavbě stadionu</a> </h4>
														<p>14.6.2017</p>

													</div>
												</div>
											</div> <!-- col xs -->
											<div class="blok dalsi">

												<div class="next">


													<a class="dalsi"href="aktuality.php#aktuality">Další</a> <br>
													<a class="odkaz" href="aktuality.php#aktuality">
														<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
													</a>
												</div>

											</div>

										</div> <!-- row -->
									</div>
								</div> <!-- AKTUALITY -->
							</div>
						</div> <!-- PRAVÝ SLOUPEC -->
					</div> <!-- row-->
				</div> <!-- content-->

















									<div class="col-xs-12 footer"> <!-- footer-->
										<div class="container-fluid whole">
											<div class="row ">
												<div class="col-xs-6">

													<div id="partneri">
														<h4>Partneři</h4>
														<div class="container-fluid">
															<div class="row ">



																<div class="col-md-6">


																	<a href="https://www.kurim.cz/"><img class="kurim"src="images/logo_kurim.png" width=100px alt="Mesto Kurim"></a>
																</div>
																<div class="col-md-6">
																	<div class="wellness">
																		<a class="big" href="http://www.wellnesskurim.cz"><img class="big" src="images/logo_wellness.png" width=200px alt="Logo Wellness Kurim"></a>

																		<a class="small" href="http://www.wellnesskurim.cz"><img class="small" src="images/logo_wellness_small.png" width=135px alt="Logo Wellness Kurim"></a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-6">
													<div class="media">
														<h4>Média</h4>
														<div class="container-fluid">
															<div class="row media">
																<div class="col-md-6">
																	<a href="http://kurimskymagazin.cz/">Kuřimský magazín</a><br>
																	<a href="http://tvkotva-kurim.cz/">TV Kotva</a><br>
																	<a href="http://www.ceskatelevize.cz/sport/">ČT Sport</a>

																</div>
																<div class="col-md-6">
																	<a href="http://www.cslh.cz/">Český svaz ledního hokeje</a> <br>
																	<a href="http://www.livesport.cz/">Live Sport</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>

                            <div class="sponzori">

                              <a href="http://en.khl.ru/"><img class="logo 2"src="images/khl.png" width=74px alt="KHL"></a>
                              <a href="https://www.nhl.com/cs"><img class="logo 3"src="images/nhl.png"  width=40px alt="NHL"></a>
                              <a href="http://www.msmt.cz/"><img class="logo 6"src="images/msmt.png" width=70px alt="MŠMT"></a>
                              <a href="http://www.hc-kometa.cz/"><img class="logo 4"src="images/kometa.png" width=60px alt="KOMETA"></a>
                              <a href="https://www.championshockeyleague.com/en"><img class="logo 5"src="images/chl.png" width=35px alt="CHL"></a>
                            </div>


									</div><!-- footer-->











								<div class="bla">
									<div class="copy">
										&copy; Zimní stadion Kuřim, 2017
									</div>
								</div>



							</div> <!-- wrap-->




						</div><!-- body-->

































						<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
						<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

						<script src="script.js"> </script>



					</body>

					</html>
